import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

// New Angular HTTPClient
import { HttpClientModule } from '@angular/common/http';

// Pages
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { AddEditHeroPage } from '../pages/add-edit-hero/add-edit-hero';
import { DetailPage } from '../pages/detail/detail';

// Ionic Native
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { TextToSpeech } from '@ionic-native/text-to-speech';

// Providers
import { RestProvider } from '../providers/rest/rest';
import { AuthHttpInterceptorProvider } from '../providers/rest/interceptor';

// Directives
import { AutosizeDirective } from '../directives/autosize/autosize';


@NgModule({
  declarations: [
    MyApp,
    HomePage,
    AddEditHeroPage,
    DetailPage,
    AutosizeDirective
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    AddEditHeroPage,
    DetailPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    TextToSpeech,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    RestProvider,
    AuthHttpInterceptorProvider
  ]
})
export class AppModule {}
