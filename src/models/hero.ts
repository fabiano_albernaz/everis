// Characters:
// attribute			type			description
// id					int				The unique ID of the character resource.
// name					string			The name of the character.
// alterEgo				string			Other name of the character.
// powers				Array[]			Powers.
// strenght				number			Strenght of the character.
// description			string			A short bio or description of the character.
// image				string			Representative image URL for this character.
// birthDate			Date			The date the character was born.
// modifiable			Bool 			Edit and Delete permision.

// Example:
// {
//   "id": "59d354a4502f29301c8c5e6c",
//   "name": "Captain Marvel",
//   "alterEgo": "Carol Danvers",
//   "powers": [
//     "Flight",
//     "Strength"
//   ],
//   "strength": 6,
//   "description": null,
//   "image": "https://i.annihil.us/u/prod/marvel/i/mg/c/10/537ba5ff07aa4/standard_xlarge.jpg",
//   "birthDate": "31/12/1980",
//   "modifiable": false
// }

import { Power } from '../models/power';

export interface Hero {
	id: number;
	name: string;
	alterEgo: string;
	birthDate: Date;
	description: string;
	image: string;
	powers: Power[];
	strength: number;
	modifiable: boolean;
}

