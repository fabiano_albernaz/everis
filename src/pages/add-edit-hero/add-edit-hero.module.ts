import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddEditHeroPage } from './add-edit-hero';

@NgModule({
  declarations: [
    AddEditHeroPage,
  ],
  imports: [
    IonicPageModule.forChild(AddEditHeroPage),
  ],
})
export class AddEditHeroPageModule {}
