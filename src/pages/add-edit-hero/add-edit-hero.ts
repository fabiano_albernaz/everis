import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, Events } from 'ionic-angular';

import { RestProvider } from '../../providers/rest/rest';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { Hero } from '../../models/hero';
import { Power } from '../../models/power';

/**
 * Generated class for the AddHeroPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
	selector: 'page-add-edit-hero',
	templateUrl: 'add-edit-hero.html',
})
export class AddEditHeroPage {

	public hero: Hero;
	public powers: Power;
	private heroForm : FormGroup;

  	constructor(
	  	public navCtrl: NavController,
	  	public navParams: NavParams, 
	  	public restProvider: RestProvider, 
	  	private formBuilder: FormBuilder,
	  	public loadingCtrl: LoadingController,
	  	public events: Events) {
			
		//Edit: Data comes from DetailPage
		this.hero = navParams.data;
		this.getPowers();
		this.createForm();
  	}

	createForm(): void {
		this.heroForm = this.formBuilder.group({
			id: [this.hero.id],
			name: [this.hero.name, Validators.required],
			alterEgo: [this.hero.alterEgo, Validators.required],
			powers: [this.hero.powers, Validators.required],
			strength: [this.hero.strength, Validators.compose([Validators.required, Validators.maxLength(1), Validators.pattern('[0-9]')])],
			description: [this.hero.description, Validators.required],
			image: [this.hero.image, Validators.required],
			birthDate: [this.hero.birthDate, Validators.compose([Validators.required, Validators.pattern('^(0[1-9]|[1-2][0-9]|3[0-1])\/(0[1-9]|1[0-2])\/[0-9]{4}$')])]
    	});
 	}

	ionViewDidLoad(): void {
		console.log('ionViewDidLoad AddHeroPage');
	}

	// Add Hero
	addHero(hero): void {
		let loading = this.loadingCtrl.create({
			spinner: 'dots',
			content: 'Creating Hero'
		});

		loading.present();

		this.restProvider.addHero(hero)
		.subscribe(hero => {
			console.log(hero);
			loading.dismiss();
			this.events.publish("updateMessage", 1);
			this.navCtrl.pop();
		}, err => {
			loading.data.content = 'An Error Occurred!';
			setTimeout(() => {loading.dismiss();}, 2000);
			console.log('Add Error:', err);
		});
	}  

	// Edit Hero
	editHero(hero): void {
		let loading = this.loadingCtrl.create({
		spinner: 'dots',
		content: 'Fixing Hero'
		});

		loading.present();

		this.restProvider.editHero(hero)
		.subscribe(hero => {
			console.log(hero);
			loading.dismiss();
			this.events.publish("updateMessage", 1);
			this.navCtrl.remove(2,1);
			this.navCtrl.pop();
		}, err => {
			loading.data.content = 'An Error Occurred!';
			setTimeout(() => {loading.dismiss();}, 2000);
			console.log('Edit Error:', err);
		});

	}

	// Get Powers
	getPowers(): void {
		this.restProvider.getPowers()
	  	.subscribe(powers => {
	  		this.powers = powers;
	    	console.log(powers);
	  	});
	}

	// Form Submit Action
	onSubmit(): void {
		if (this.hero.id == undefined) {
	    	this.addHero(this.heroForm.value);
		} else {
	  		this.editHero(this.heroForm.value);
		}
	}

}
