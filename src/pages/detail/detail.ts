import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events } from 'ionic-angular';
import { Platform } from 'ionic-angular';
import { AlertController } from 'ionic-angular';

import { TextToSpeech } from '@ionic-native/text-to-speech';

import { RestProvider } from '../../providers/rest/rest';

import { AddEditHeroPage } from '../add-edit-hero/add-edit-hero';

import { Hero } from '../../models/hero';

/**
 * Generated class for the DescriptionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-detail',
    templateUrl: 'detail.html',
})
export class DetailPage {
    public id;
    public hero: Hero;

    constructor(
      public platform: Platform,
      public navCtrl: NavController,
      public navParams: NavParams,
      public restProvider: RestProvider,
      private alertCtrl: AlertController,
      public events: Events,
      private tts: TextToSpeech) {
      // this.id = navParams.get("id");
      //console.log(this.id)
      this.hero = navParams.data;
      console.log(this.hero);
    }

    ionViewDidLoad() {
      console.log('ionViewDidLoad DescriptionPage');
      this.speak(this.hero.name);
      this.speak(this.hero.description);
    }

    // getHero(): void {
    //   this.hero = new Hero();
    //   this.restProvider.getHero(this.id)
    //   .subscribe(hero => {
    //     this.hero = hero;
    //     console.log(this.hero);
    //   });
    // }

    deleteConfirm(): void {
        let alert = this.alertCtrl.create({
          title: 'Confirm Delete Hero',
          message: 'Do you really want to kill this Hero?',
          buttons: [
            {
              text: 'Cancel',
              role: 'cancel',
              handler: () => {
                console.log('Cancel clicked');
              }
            },
            {
              text: 'Delete',
              handler: () => {
                this.deleteHero(this.hero.id);
              }
            }
          ]
        });
        alert.present();
    }   

    deleteHero(id: number): void {
        this.restProvider.deleteHero(id)
        .subscribe(id => {
          console.log('Hero set to '+id);
          this.events.publish('updateMessage', 1);
          // Navigate 
          this.navCtrl.pop();
        });
    } 

    editHero(hero: Hero): void {
        // Navigate 
        this.navCtrl.push(AddEditHeroPage, hero);
    }

    speak(text): void {
        let options: any;

        if (this.platform.is('ios')) {
          options = {text: text, locale:'en-US', rate: 1.5};
        } else {
          options = {text: text, locale:'en-US', rate: 1};
        }

        this.tts.speak(options)
        .then(() => console.log('Success'))
        .catch((reason: any) => console.log(reason));
    }

    ionViewWillLeave() {
        // Stop Speaking
        this.tts.speak('');
    }

}