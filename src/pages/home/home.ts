import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, Events } from 'ionic-angular';

import { RestProvider } from '../../providers/rest/rest';

import { DetailPage } from '../detail/detail';
import { AddEditHeroPage } from '../add-edit-hero/add-edit-hero';

import { Hero } from '../../models/hero';

@Component({
    selector: 'page-home',
    templateUrl: 'home.html',
})
export class HomePage {

    hero: Hero;
    heroes: Hero[];
    list: Hero[];
    reverse: boolean;

    constructor(
        public navCtrl: NavController, 
        public navParams: NavParams, 
        public restProvider: RestProvider, 
        public loadingCtrl: LoadingController,
        public events: Events) {
        console.log('Hello Home Constructor');
        // Sharing data with Emitter. 
        // It works only if receiving page has aleady been loaded. 
        // So in this case I'm sending it from the Second Page to the Home Page (backwards)
        events.subscribe('updateMessage', (needUpdate) => {
          if(needUpdate)
            console.log('updating...');
            this.getHeroes('Updating Heroes');
        });  
  }

    ionViewDidLoad() {
        console.log('ionViewDidLoad HomePage');
        this.getHeroes('Calling Heroes');
    }

    getHeroes(actionString: string): void {
        let loading = this.loadingCtrl.create({
            spinner: 'dots',
            content: actionString
        });

        loading.present();

        this.restProvider.getHeroes()
        .subscribe(heroes => {
            this.heroes = heroes;
            this.list = heroes;
            this.orderHeroes();
            console.log(this.heroes);
            loading.dismiss();
        });
    }

    filterHeroes(ev: any): void {
        // Reset heroes back to all of the heroes
        this.heroes = this.list;

        // Set val to the value of the searchbar
        let val = ev.target.value;

        // If the value is an empty string don't filter the heroes
        if (val && val.trim() != '') {
          this.heroes = this.heroes.filter((user) => {
            return (user.name.toLowerCase().indexOf(val.toLowerCase()) > -1);
          })
        }
    }

    orderHeroes(): void {
        // array.sort(compareFunction)
        this.heroes.sort(function(foo, bar) {
          if (foo.name < bar.name) {
            return -1;
          } else if (foo.name > bar.name) {
            return 1;
          } else {
            return 0;  
          }
        });
        this.reverse = false;
    }

    reverseHeroes(): void {
        if (!this.reverse) {
          this.heroes.reverse();
          this.reverse = true;
        }
    }

    getHero(hero: Hero) {
        this.navCtrl.push(DetailPage, hero);
    }

    showAddHeroPage() {  
        this.navCtrl.push(AddEditHeroPage);
    }

    doRefresh(refresher) {
        console.log('Begin async operation', refresher);
        
        this.restProvider.getHeroes()
        .subscribe(heroes => {
          this.heroes = heroes;
          this.orderHeroes();
          console.log(this.heroes);
          console.log('Async operation has ended');
          refresher.complete();
        });
    }
}
