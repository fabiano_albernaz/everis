import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpResponse, HttpErrorResponse, HTTP_INTERCEPTORS } from '@angular/common/http';

// Rxjs
import { Observable } from "rxjs/Observable";
import 'rxjs/add/operator/do';


/**
 * Intercepts the HTTP responses, and in case that an error/exception is thrown, handles it
 * and extract the relevant information of it.
 */
@Injectable()
export class AuthHttpInterceptor implements HttpInterceptor {

	/**
	* Intercepts an outgoing HTTP request, executes it and handles any error that could be triggered in execution.
	* @see HttpInterceptor
	* @param req the outgoing HTTP request
	* @param next a HTTP request handler
	*/
	intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

	// Clone the request to add the new header.
	const authReq = req.clone({headers: req.headers
		.set("Accept", "*/*")
		.set("Content-Type", "application/json")
		.set("userName", "fabiano.albernaz")});

	return next.handle(authReq).do((event: HttpEvent<any>) => {
	  	if (event instanceof HttpResponse) {
		    // Logging response in console when request happened.
		    const time = new Date().toLocaleString();
		    console.log(event);
		    console.log(`Request happened at ${time}.`); 
	  	}
	}, (errorReponse: any) => {
			let statusText: string;
			let errorMessage: string;
			if (errorReponse instanceof HttpErrorResponse) {

				const error = errorReponse.message || JSON.stringify(errorReponse.error);
				
				if (errorReponse.status === 204) {
					statusText = 'No Content';
				}
				if (errorReponse.status === 400) {
					statusText = 'Bad Request';
				}
				if (errorReponse.status === 401) {
					statusText = 'Unauthorized';
				}
				if (errorReponse.status === 404) {
					statusText = 'Not Found';
				}
				if (errorReponse.status === 405) {
					statusText = 'Method Not Allowed';
				}
				if (errorReponse.status === 500) {
					statusText = 'Internal Server Error';
				}

				errorMessage = `${errorReponse.status} - ${statusText || ''} Details: ${error}`;
				console.log('HTTP ERROR', errorMessage);
		  	} else {
			    errorMessage = errorReponse.message ? errorReponse.message : errorReponse.toString();
			}
		});
	}
}

//Provider POJO for the interceptor
export const AuthHttpInterceptorProvider = {
    provide: HTTP_INTERCEPTORS,
    useClass: AuthHttpInterceptor,
    multi: true,
};