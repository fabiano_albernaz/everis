import { Injectable } from '@angular/core';

// New Angular HttpClient. More info at: https://angular.io/guide/http 
import { HttpClient } from '@angular/common/http';

// Rxjs
import { Observable } from "rxjs/Observable";

// Models
import { Hero } from "../../models/hero";
import { Power } from "../../models/power";

/*
  Generated class for the RestProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class RestProvider {

  // Base URL to web api
  private readonly apiUrl = 'https://btools-challenge.herokuapp.com/challenge';

  constructor(public http: HttpClient) {
    console.log('Hello RestProvider Provider');
  }

  addHero (hero: Hero): Observable<Hero> {
    const url = `${this.apiUrl}/character`;
    const body = hero;

    // Post add request (url, body)
    return this.http.post<Hero>(url, body);
  }

  deleteHero(id: number): Observable<Hero> {
    const url = `${this.apiUrl}/character/${id}`;

    // Delete request (url)        
    return this.http.delete<Hero>(url);
  }
  
  editHero (hero: Hero): Observable<Hero> {
    const url = `${this.apiUrl}/character`;
    const body = hero;

    // Put request (url, body)
    return this.http.put<Hero>(url, body);
  }

  getHero(id: number): Observable<Hero> {
    const url = `${this.apiUrl}/character/${id}`;

    // Get request (url)        
    return this.http.get<Hero>(url);
  }

  getHeroes (): Observable<Hero[]> {
    const url = `${this.apiUrl}/character/search`;
    const body = JSON.stringify({});

    // Post request (url, body)
    return this.http.post<Hero[]>(url, body);
  }

  getPowers (): Observable<Power[]> {
    const url = `${this.apiUrl}/power/search`;

    // Get request (url)        
    return this.http.get<Power[]>(url);
  }

}